<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\ContactType;
use App\Repository\BlogRepository;
use App\Repository\ContentRepository;
use App\Repository\FAQRepository;
use App\Repository\ImageRepository;
use App\Repository\PageRepository;
use App\Repository\TeamMemberRepository;
use App\Repository\TestimonialsRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class IndexController extends AbstractController
{

    public function __construct(private EntityManagerInterface $em, private readonly Mailer $mailer){}

    #[Route('/', name: 'app_index')]
    public function index(BlogRepository $blogRepository, FAQRepository $fAQRepository, TestimonialsRepository $testimonialsRepository, TeamMemberRepository $teamMemberRepository, ContentRepository $contentRepository, PageRepository $pageRepository, ImageRepository $imageRepository): Response
    {
        $all_content = $contentRepository->findAll();
        $all_images = $imageRepository->findAll();
        $content = [];
        $images = [];
        $index = $pageRepository->findOneByUrl('natural-healthy-living');
        $services = $pageRepository->findByIsService(true);
        $teamMembers = $teamMemberRepository->findAll();
        $testimonials = $testimonialsRepository->findAll();
        $blog_posts = $blogRepository->findPublished(true, 3);
        $faqs = $fAQRepository->findBy(
            [],
            ['id' => 'asc'],
            3
        );

        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }
        foreach ($all_images as $item) {
            $images[$item->getSlug()] = $item;
        }
        return $this->render('index/index.html.twig', [
            'content' => $content,
            'page' => $index,
            'images' => $images,
            'services' => $services,
            'teamMembers' => $teamMembers,
            'testimonials' => $testimonials,
            'faqs' => $faqs,
            'blog_posts' => $blog_posts
        ]);
    }

    #[Route('/service/{service}', name: 'app_service')]
    public function service(ContentRepository $contentRepository, PageRepository $pageRepository, $service): Response
    {
        $all_content = $contentRepository->findAll();
        $content = [];
        $service_page = $pageRepository->findOneByUrl($service);
        $services = $pageRepository->findByIsService(true);
        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }

       return $this->render('index/service.html.twig', [
            'content' => $content,
            'page' => $service_page,
            'services' => $services,
        ]);
    }

    #[Route('/post/{slug}', name: 'blog_detail')]
    public function blog(BlogRepository $blogRepository, ImageRepository $imageRepository, ContentRepository $contentRepository, PageRepository $pageRepository, $slug): Response
    {
        $blog_post = $blogRepository->findOneBySlug($slug);
        $title = $blog_post->getTitle();
        $all_content = $contentRepository->findAll();
        $all_images = $imageRepository->findAll();
        $images = [];
        foreach ($all_images as $item) {
            $images[$item->getSlug()] = $item;
        }

        $content = [];
        $blog_page = $pageRepository->findOneByUrl('blog-content');
        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }
        $blog_page->setMetaDescription($blog_post->getMetaDescription());
        $blog_page->setMetaTitle($blog_post->getMetaTitle());

        return $this->render('index/page.html.twig', [
            'blog_post' => $blog_post,
            'page' => $blog_page,
            'content' => $content,
            'images' => $images,
            'title' => $title,
        ]);
    }

    public function renderFooterForm(): Response
    {
        $form = $this->createForm(ContactType::class);
        
        return $this->render('tmp/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/contact-form', name: 'contact_form')]
    public function contact_form(UserRepository $userRepo, Request $request): Response
    {

        // Create an instance of the form
        $form = $this->createForm(ContactType::class);
        
        // Handle form submission
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact = $form->getData();

            $name = $contact['name'];
            $email = $contact['email'];
            $subject = $contact['subject'];
            $msg = $contact['message'];
            $date = new \DateTimeImmutable();

            
            $message = new Message();
            $message->setName($name);
            $message->setEmail($email);
            $message->setSubject($subject);
            $message->setMessage($msg);
            $message->setReceivedOn($date);
            $this->em->persist($message);
            $this->em->flush();

            $subject = "Message From Contact Form";
            $admins = $userRepo->findByUsertype('admin');
            $maildata = ['name' => $name, 'emailAd' => $email, 'subject' => $subject, 'message' => $msg];

            foreach ($admins as $admin) {
                $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "contactform.html.twig");
            }
            $this->mailer->sendEmailMessage($maildata, $email, "Your message - ". $subject . " - received", "contactreceived.html.twig");
    
            $this->addFlash('success','Your Message has been sent');

        } else {

            $this->addFlash('danger','Your Message has NOT been sent');

        }
   
        return $this->redirectToRoute('app_index');

    }

    #[Route('/{page}', name: 'app_page')]
    public function page(TestimonialsRepository $testimonialsRepository, BlogRepository $blogRepository, FAQRepository $fAQRepository, ContentRepository $contentRepository, PageRepository $pageRepository, ImageRepository $imageRepository, $page): Response
    {
        $all_content = $contentRepository->findAll();
        $content = [];
        $page = $pageRepository->findOneByUrl($page);
        $services = $pageRepository->findByIsService(true);
        $testimonials = $testimonialsRepository->findAll();
        $all_images = $imageRepository->findAll();
        $title = $page->getHeading();
        $faqs = $fAQRepository->findAll();
        $blog_posts = $blogRepository->findPublished(true, 50);
        $images = [];
        foreach ($all_images as $item) {
            $images[$item->getSlug()] = $item;
        }

        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }
        return $this->render('index/page.html.twig', [
            'content' => $content,
            'page' => $page,
            'services' => $services,
            'images' => $images,
            'title' => $title,
            'faqs' => $faqs,
            'testimonials' => $testimonials,
            'blog_posts' => $blog_posts,
        ]);
    }

}
