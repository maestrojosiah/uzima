<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel("User Information");
        yield TextField::new("first_name")->setLabel("First Name");
        yield TextField::new("last_name")->setLabel("Last Name");
        yield ChoiceField::new("usertype")->setLabel("Usertype")
            ->setChoices([
                'User' => 'user',
                'Admin' => 'admin',
            ])
            ->setPermission("ROLE_USER");//Change to Admin
        yield ImageField::new("profile_picture")->setLabel("Photo")
            ->setUploadDir("/public/site/images/profile_pictures")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/profile_pictures");
        yield BooleanField::new("is_active")->setLabel("Active?")
            ->setPermission("ROLE_USER");//Change to Admin
        yield EmailField::new("email")->setLabel("Email");
        $roles = ['ROLE_ADMIN', 'ROLE_USER'];
        yield ChoiceField::new('roles')
            ->setChoices(array_combine($roles, $roles))
            ->allowMultipleChoices()
            ->renderAsBadges()
            ->renderExpanded();    
        yield TextField::new('password')
            ->setRequired($pageName === Crud::PAGE_NEW)
            ->onlyWhenCreating();

        yield FormField::addColumn(6)->setLabel("More Information");
        yield TextareaField::new("about")->setLabel("About Me")
            ->onlyOnForms();

    }

}
