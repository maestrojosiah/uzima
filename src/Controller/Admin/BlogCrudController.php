<?php

namespace App\Controller\Admin;

use App\Entity\Blog;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class BlogCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Blog::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $blog = new Blog();
        $blog->setAuthor($this->getUser());
        $blog->setPublicationDate(new \DateTimeImmutable());

        return $blog;
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel("Blog Information");
        yield TextField::new("title")->setLabel("Title");
        yield TextField::new("slug")->setLabel("Url Slug");
        yield TextareaField::new("content")->setLabel("Content")
            ->setFormType(CKEditorType::class)
            ->onlyOnForms();
        
        yield FormField::addColumn(6)->setLabel("More Blog Information");
        yield ImageField::new("featured_image")->setLabel("Featured Image")
            ->setUploadDir("/public/site/images/blog_images")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/blog_images")
            ->setHelp("1542*996");
        yield TextField::new("meta_title")->setLabel("Meta Title");
        yield TextField::new("meta_description")->setLabel("Meta Description");
        yield BooleanField::new("is_published")->setLabel("Published?");
        yield AssociationField::new("blogCategories")->setLabel("Categories")
            ->autocomplete()
            ->setFormTypeOptions([
                'by_reference' => false,
            ]); 
            

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
        ;
    }    

}
