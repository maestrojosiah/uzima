<?php

namespace App\Controller\Admin;

use App\Entity\Page;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class PageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Page::class;
    }


    public function configureFields(string $pageName): iterable
    {
        
        yield FormField::addTab("Page With Optional Sections");
        yield FormField::addColumn(6)->setLabel("Page Information");
        yield TextField::new("heading")->setLabel("Page Title");
        yield TextField::new("meta_title")->setLabel("SERP Title");
        yield TextField::new("meta_description")->setLabel("SERP Description");
        yield TextField::new("url")->setLabel("Slug");
        yield AssociationField::new("sections")->setLabel("Sections")
            ->autocomplete()
            ->setFormTypeOptions([
                'by_reference' => false,
            ]);    
        yield ImageField::new("featured_image")->setLabel("Featured Image")
            ->setUploadDir("/public/site/images/page_images")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/page_images");


        yield FormField::addTab("Add Content");
        yield TextareaField::new("page_content")->setLabel("Content")
            ->setFormType(CKEditorType::class)
            ->onlyOnForms()
            ->setHelp("Leave Content Blank if your page is using only the sections you've added on the first tab.");
        yield BooleanField::new("content_first")->setLabel("Content First?")
            ->setHelp("Check if the content you're adding should come before the sections that you have added.");
        yield BooleanField::new("is_service")->setLabel("Is Service?")
            ->setHelp("Check if this page is a service page.");

        
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
        ;
    }    


}
