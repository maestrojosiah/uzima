<?php

namespace App\Controller\Admin;

use App\Entity\Testimonials;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class TestimonialsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Testimonials::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel("Testimonial Information");
        yield TextField::new("name")->setLabel("Name");
        yield TextField::new("title")->setLabel("Title");
        yield TextareaField::new("content")->setLabel("Content");
        yield ImageField::new("photo")->setLabel("Photo")
            ->setUploadDir("/public/site/images/testimonial_photos")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/testimonial_photos");
    }

}
