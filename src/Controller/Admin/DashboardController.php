<?php

namespace App\Controller\Admin;

use App\Entity\Blog;
use App\Entity\BlogCategory;
use App\Entity\Content;
use App\Entity\FAQ;
use App\Entity\Image;
use App\Entity\Message;
use App\Entity\Page;
use App\Entity\Section;
use App\Entity\SEO;
use App\Entity\TeamMember;
use App\Entity\Testimonials;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
class DashboardController extends AbstractDashboardController
{

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        // return parent::index();
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'DashboardController',
        ]);

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Uzima Well-being Hub')
            ->setFaviconPath('/favicon.ico');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-dashboard');
        yield MenuItem::linkToCrud('Blog', 'fas fa-pen', Blog::class);
        yield MenuItem::linkToCrud('Blog Category', 'fas fa-folder', BlogCategory::class);
        yield MenuItem::linkToCrud('Web Content', 'fas fa-globe', Content::class);
        yield MenuItem::linkToCrud('Images', 'fas fa-image', Image::class);
        yield MenuItem::linkToCrud('Sections', 'fas fa-columns', Section::class);
        yield MenuItem::linkToCrud('Pages', 'fas fa-file', Page::class);
        yield MenuItem::linkToCrud('SEO', 'fas fa-search', SEO::class);
        yield MenuItem::linkToCrud('Team Members', 'fas fa-users', TeamMember::class);
        yield MenuItem::linkToCrud('Testimonials', 'fas fa-comment', Testimonials::class);
        yield MenuItem::linkToCrud('FAQ', 'fas fa-question-circle', FAQ::class);
        yield MenuItem::linkToCrud('User', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Messages', 'fas fa-envelope', Message::class);
        yield MenuItem::linkToUrl('Homepage', 'fa fa-home', $this->generateUrl('app_index'));
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }

    public function configureActions(): Actions
    {
        return parent::configureActions()
            ->add(Crud::PAGE_INDEX, Action::DETAIL) ;
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        if (!$user instanceof User) {
            throw new \Exception('Wrong user');
        }

        return parent::configureUserMenu($user)
            ->setAvatarUrl("/site/images/profile_pictures/".$user->getAvatarUri())
            ->setMenuItems([
                // MenuItem::linkToUrl('My Profile','fas fa-user', $this->generateUrl('app_profile_show')),
                MenuItem::linkToUrl('Logout', 'fa fa-sign-out', $this->generateUrl('app_logout')),
            ]);

    }

    // public function configureAssets(): Assets
    // {
    //     return parent::configureAssets()
    //         ->addCssFile('/site/css/test.css'); 
    // }

    public function configureCrud(): Crud
    {
        return parent::configureCrud()
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setPageTitle('index', '%entity_label_plural% listing')
            ->showEntityActionsInlined();
    }


}
