<?php

namespace App\Twig;

use App\Manager\CartManager;
use App\Repository\BlogRepository;
use App\Repository\FAQRepository;
use App\Repository\SEORepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\ProductRepository;
use App\Entity\Orderr;
use App\Repository\OrderrRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class GlobalVars extends AbstractExtension
{
    public function __construct(private UserRepository $usr, private SEORepository $seoRepo, private Security $security, private BlogRepository $blogRepository, private FAQRepository $faqRepo)
    {
    }

    public function getFunctions(): ?array
    {
        return [new TwigFunction('global_vars', $this->getGlobalVars(...))];
    }

    public function getGlobalVars($select)
    {
        $seo = $this->seoRepo->findAll();
        $user = $this->getUserFromSecurityContext();
        $blog_posts = $this->blogRepository->findPublished(true, 3);
        $faqs = $this->faqRepo->findAll();

        $url = parse_url((string) $_SERVER['REQUEST_URI']);

        $glVars = [];
        $glVars['seo'] = $seo[0];
        $glVars['posts'] = $blog_posts;
        $glVars['faqs'] = $faqs;
        
        // $glVars['thisUrl'] = $url['path'];

        return $glVars[$select];

    }

     /**
     * Gets the currently logged-in user from the security context.
     *
     * @return UserInterface|null
     */
    private function getUserFromSecurityContext(): ?UserInterface
    {
        $token = $this->getSecurityToken();

        if ($token && $token->getUser() instanceof UserInterface) {
            return $token->getUser();
        }

        return null;
    }

    /**
     * Gets the security token from the security context.
     *
     * @return TokenInterface|null
     */
    private function getSecurityToken(): ?TokenInterface
    {
        return $this->security->getToken();
    }


}