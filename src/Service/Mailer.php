<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use App\Entity\User;
use App\Repository\SettingsRepository;
use App\Repository\UserRepository;
use Knp\Snappy\Pdf;
use Dompdf\Dompdf;
use Twig\Environment;

class Mailer
{
    public function __construct(private readonly MailerInterface $mailer, private readonly Environment $twig, private readonly UserRepository $userRepo)
    {
    }

    public function sendEmailMessage($data, $sendto, $subject, $template)
    {
        
        $email = (new TemplatedEmail())
            ->from(new Address('info@uzimahub.co.ke', 'Uzima Well-being Hub'))
            ->to(new Address($sendto))
            ->subject($subject)

            // path of the Twig template to render
            ->htmlTemplate('emails/'.$template)

            // pass variables (name => value) to the template
            ->context($data);

            $this->mailer->send($email);
            // echo "sending email<br>";

        return true;

    }


}
