-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: localhost    Database: uzima
-- ------------------------------------------------------
-- Server version	8.0.36-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `publication_date` datetime DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `featured_image` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `author_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C0155143F675F31B` (`author_id`),
  CONSTRAINT `FK_C0155143F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (1,'Baobab Powder: A Natural Source of Calcium for Optimal Health','<p>In the world of superfoods, few stand as tall, both literally and figuratively, as the mighty baobab tree. Revered across Africa for centuries, this iconic tree is not just a botanical wonder but also a powerhouse of nutrients. Among its many gifts to humankind, baobab powder emerges as a standout, celebrated for its impressive calcium content and the myriad health benefits it bestows upon those who incorporate it into their diets.</p>\r\n\r\n<h3>The Baobab: A Symbol of Resilience and Nutrient Abundance</h3>\r\n\r\n<p>Before delving into the specifics of baobab powder, let&#39;s take a moment to appreciate the tree from which it comes. Often referred to as the &quot;Tree of Life&quot; or the &quot;Upside-Down Tree&quot; due to its distinctive appearance, the baobab (Adansonia) is native to the African continent and has a rich cultural significance in many African societies.</p>\r\n\r\n<p>Despite its massive trunk and gnarled branches, the baobab is surprisingly resilient, able to thrive in harsh, arid environments where other plants struggle to survive. Its fruits, known as baobab fruit or &quot;monkey bread,&quot; hang from the branches and contain a powdery pulp that has been treasured by indigenous peoples for its nutritional value and medicinal properties.</p>\r\n\r\n<h3>Unpacking the Nutritional Treasure Trove: Baobab Powder</h3>\r\n\r\n<p>Baobab powder is derived from the dried pulp of the baobab fruit. This fine, pale powder boasts an impressive nutritional profile, making it a sought-after addition to health-conscious diets worldwide. Among its many nutrients, calcium shines as one of the most notable.</p>\r\n\r\n<h3>The Calcium Connection: Why It Matters</h3>\r\n\r\n<p>Calcium is an essential mineral that plays a crucial role in various bodily functions. While it&#39;s widely known for its role in promoting strong bones and teeth, calcium is also involved in muscle function, nerve transmission, hormone secretion, and blood clotting. Insufficient calcium intake can lead to a host of health issues, including osteoporosis, a condition characterized by brittle bones and an increased risk of fractures.</p>\r\n\r\n<h3>Baobab Powder: A Calcium-Rich Superfood</h3>\r\n\r\n<p>What sets baobab powder apart is its remarkably high calcium content. Just a single tablespoon of baobab powder can provide a significant portion of your daily calcium needs, making it an excellent choice for individuals looking to boost their calcium intake without relying solely on dairy products or supplements.</p>\r\n\r\n<h3>The Benefits of Baobab Powder Beyond Calcium</h3>\r\n\r\n<p>While its calcium content is certainly impressive, baobab powder offers a wealth of additional health benefits:</p>\r\n\r\n<h4>Rich in Antioxidants:</h4>\r\n\r\n<p>Baobab powder is loaded with antioxidants, including vitamin C, which helps combat oxidative stress and inflammation in the body. Antioxidants play a crucial role in protecting cells from damage caused by free radicals, thereby supporting overall health and well-being.</p>\r\n\r\n<h4>Gut Health Support:</h4>\r\n\r\n<p>Thanks to its high fiber content, baobab powder can promote digestive health by supporting regularity and feeding beneficial gut bacteria. Fiber also helps to stabilize blood sugar levels and promote satiety, making it a valuable addition to weight management plans.</p>\r\n\r\n<h4>Immune System Boost:</h4>\r\n\r\n<p>The vitamin C content in baobab powder contributes to a robust immune system, helping to fend off infections and illnesses. By incorporating baobab powder into your diet, you can give your body an extra line of defense against colds, flu, and other common ailments.</p>\r\n\r\n<h3>How to Incorporate Baobab Powder Into Your Diet</h3>\r\n\r\n<p>Adding baobab powder to your daily routine is simple and versatile. You can mix it into smoothies, yogurt, oatmeal, or baked goods for a nutritional boost. Its subtly tangy flavor adds a unique twist to recipes, making it a favorite among culinary enthusiasts.</p>\r\n\r\n<h3>Embracing the Baobab: A Natural Path to Wellness</h3>\r\n\r\n<p>In a world where processed foods and synthetic supplements abound, the allure of natural, nutrient-rich superfoods like baobab powder is undeniable. With its exceptional calcium content and array of health-promoting properties, baobab powder offers a holistic approach to wellness that honors both the body and the planet.</p>\r\n\r\n<p>So, whether you&#39;re seeking to fortify your bones, support your immune system, or simply nourish your body with the best nature has to offer, consider harnessing the power of baobab powder and embarking on a journey toward optimal health and vitality.</p>\r\n\r\n<p>Incorporate this ancient African treasure into your daily routine and discover firsthand the transformative benefits it can bring to your life. After all, when it comes to health and well-being, sometimes the most profound solutions are found in the simplest of places&mdash;the natural world around us.</p>','2024-03-28 06:58:03',1,'baobab-powder-calcium-benefits','baobab-tree-powder-1711609083.jpg','Baobab Powder - Source of Calcium','Among its many gifts to humankind, baobab powder emerges as a standout, celebrated for its impressive calcium content and the myriad health benefits it bestows upon those who incorporate it into their diets',1),(2,'Wound Care - Discover The Healing Power of Cayenne Pepper','<p>In the realm of traditional medicine, natural remedies have long been valued for their healing properties. One such remedy that has gained attention for its potential in wound care is cayenne pepper.</p>\r\n\r\n<p>While commonly known for its culinary use, cayenne pepper possesses remarkable medicinal properties that can aid in wound healing and alleviate discomfort.</p>\r\n\r\n<p>Let&#39;s delve into the fascinating world of cayenne pepper and its applications in wound management.</p>\r\n\r\n<h2>The Healing Potential of Cayenne Pepper</h2>\r\n\r\n<p>Cayenne pepper, derived from the Capsicum annuum plant, is renowned for its active component, capsaicin.</p>\r\n\r\n<p>This compound is responsible for the pepper&#39;s characteristic spicy flavor and also contributes to its therapeutic effects.</p>\r\n\r\n<p>Capsaicin exhibits analgesic, anti-inflammatory, and antimicrobial properties, making it a versatile remedy for various ailments, including wounds.</p>\r\n\r\n<h3>Pain Relief</h3>\r\n\r\n<p>One of the primary benefits of cayenne pepper in wound care is its ability to alleviate pain.</p>\r\n\r\n<p>Capsaicin works by desensitizing nerve receptors responsible for transmitting pain signals to the brain. When applied topically to a wound, cayenne pepper cream or ointment containing capsaicin can effectively reduce pain and discomfort, providing relief to the affected individual.</p>\r\n\r\n<h3>Anti-inflammatory Properties</h3>\r\n\r\n<p>Inflammation is a natural response of the body to injury or infection, but excessive inflammation can impede the healing process.</p>\r\n\r\n<p>Cayenne pepper&#39;s anti-inflammatory properties help mitigate inflammation at the wound site, promoting faster healing.</p>\r\n\r\n<p>By reducing swelling and redness, cayenne pepper aids in restoring the damaged tissue and minimizing the risk of complications.</p>\r\n\r\n<h3>Antimicrobial Action</h3>\r\n\r\n<p>Another crucial aspect of wound care is preventing infection, which can delay healing and lead to further complications.</p>\r\n\r\n<p>Cayenne pepper possesses antimicrobial properties that help combat bacteria and other pathogens present in the wound.</p>\r\n\r\n<p>The antibacterial effects of capsaicin inhibit the growth of microorganisms, thereby reducing the risk of infection and supporting the body&#39;s natural healing mechanisms.</p>\r\n\r\n<h3>Stimulating Blood Circulation</h3>\r\n\r\n<p>Optimal blood circulation is essential for delivering oxygen and nutrients to the wound site, facilitating tissue repair and regeneration.</p>\r\n\r\n<p>Cayenne pepper is known for its vasodilatory effects, meaning it widens blood vessels and promotes blood flow.</p>\r\n\r\n<p>By enhancing circulation, cayenne pepper aids in the removal of toxins and promotes the delivery of healing agents to the wound, accelerating the healing process.</p>\r\n\r\n<h2>How to Use Cayenne Pepper for Wounds</h2>\r\n\r\n<p>Cayenne pepper can be utilized in various forms for wound care, including creams, ointments, and poultices.</p>\r\n\r\n<p>These preparations are readily available at health food stores or can be prepared at home using natural ingredients.</p>\r\n\r\n<p>When applying cayenne pepper topically, it&#39;s essential to dilute it properly to avoid irritation or burning sensation, especially on sensitive skin.</p>\r\n\r\n<p>To create a cayenne pepper poultice, mix powdered cayenne pepper with a neutral base such as olive oil or coconut oil to form a paste.</p>\r\n\r\n<p>Apply the poultice directly to the wound and cover it with a clean bandage.</p>\r\n\r\n<p>Leave it on for a few hours or overnight, then rinse thoroughly with lukewarm water.</p>\r\n\r\n<p>Repeat this process as needed until the wound heals.</p>\r\n\r\n<h2>Precautions and Considerations</h2>\r\n\r\n<p>While cayenne pepper offers several benefits for wound care, it&#39;s essential to exercise caution when using it, especially on open wounds or broken skin. Some individuals may experience allergic reactions or skin irritation when exposed to capsaicin.</p>\r\n\r\n<p>It&#39;s advisable to perform a patch test before applying cayenne pepper preparations to a larger area of the skin.</p>\r\n\r\n<p>Additionally, cayenne pepper should not be used on deep or puncture wounds without consulting a healthcare professional.</p>\r\n\r\n<p>In some cases, it may be contraindicated, particularly for individuals with certain medical conditions or those taking medications that interact with capsaicin.</p>\r\n\r\n<p>In conclusion, cayenne pepper&#39;s healing properties make it a valuable asset in wound care and management.</p>\r\n\r\n<p>From pain relief and inflammation reduction to antimicrobial action and improved blood circulation, cayenne pepper offers a holistic approach to promoting wound healing.</p>\r\n\r\n<p>When used appropriately and with proper precautions, cayenne pepper can be an effective natural remedy for minor wounds and injuries, contributing to faster recovery and improved well-being.</p>\r\n\r\n<p>Please note that Cayenne pepper is in high demand and most bulk sellers have contaminated it with other substances. Please reach out to us if you&#39;d like to know where to get original Cayenne pepper in Nairobi.</p>','2024-04-01 08:42:49',1,'cayenne-pepper-for-wound-care','cayenne-pepper-wound-care-1711960969.jpg','Wound Care - The Healing Power of Cayenne Pepper','Discover the healing power of cayenne pepper for wounds: pain relief, inflammation reduction, and faster healing. Learn safe usage and benefits',1),(3,'Safeguarding Your Health and Wealth for the Future - Preventive Measures','<p>In life, there comes a time when we look forward to reaping the rewards of our hard-earned wealth and enjoying the fruits of our labor. However, what many fail to realize is that the key to safeguarding our financial stability in our later years lies not only in accumulating wealth but also in preserving our health through preventive measures.</p>\r\n\r\n<h3>What we realize when it is too late</h3>\r\n\r\n<p>As we age, our bodies naturally undergo changes, making us more susceptible to various health conditions and diseases. The cost of healthcare can be exorbitant, especially when faced with the need for medical treatments, surgeries, and long-term care. This is where the importance of preventive measures becomes paramount.</p>\r\n\r\n<p>Preventive measures, such as healthy lifestyle choices, regular health screenings, subscribing to health education providers, and early intervention, play a crucial role in maintaining optimal health and well-being. By taking proactive steps to prevent illness and disease, we not only protect our health but also safeguard our finances from the burden of expensive medical expenses.</p>\r\n\r\n<h3>When You Get To The 4th Floor</h3>\r\n\r\n<p>Consider this scenario: You&#39;ve worked hard your entire life, diligently saving and investing to secure your financial future. However, as you approach your mid-40s, you&#39;re suddenly faced with a serious health condition that requires costly medical treatments and ongoing care. Without adequate preventive measures in place, you may find yourself depleting your savings and assets to cover these unexpected expenses, jeopardizing the financial security you&#39;ve worked so hard to achieve.</p>\r\n\r\n<h3>Make Healthy Choices Now</h3>\r\n\r\n<p>On the other hand, by prioritizing preventive healthcare throughout your life, you can significantly reduce the likelihood of developing chronic illnesses and age-related conditions. Regular health check-ups, screenings, and preventive interventions allow for early detection and treatment of potential health issues before they escalate into serious problems.</p>\r\n\r\n<p>Moreover, adopting a healthy lifestyle, including maintaining a balanced diet, engaging in regular exercise, managing stress, and avoiding harmful habits such as smoking and alcohol consumption, can further reduce your risk of developing chronic diseases such as heart disease, diabetes, and certain cancers.</p>\r\n\r\n<p>Investing in preventive healthcare is not only beneficial for individuals but also for society as a whole. By promoting a culture of prevention, we can alleviate the burden on healthcare systems, reduce healthcare costs, and improve overall population health outcomes.</p>\r\n\r\n<p>In essence, taking preventive measures is an investment in <strong>both your health and your wealth</strong>. By prioritizing preventive healthcare throughout your life, you can enjoy a longer, healthier, and more financially secure future. So, why wait until health concerns arise to take action? Start taking preventive measures today and pave the way for a brighter tomorrow.</p>\r\n\r\n<p>Remember, an ounce of prevention is worth a pound of cure. Take control of your health, protect your wealth, and embrace the power of preventive measures to safeguard your future well-being. Your health and financial future are worth it.</p>','2024-04-01 10:34:17',1,'safeguarding-your-future-health-and-wealth','healthy-vs-junk-uzima-hub-1711967657.jpg','Safeguarding Your Health and Wealth for the Future','Discover the importance of preventive measures in safeguarding both your health and wealth as you age. Prioritize prevention for a brighter future.',1);
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_category`
--

DROP TABLE IF EXISTS `blog_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_category`
--

LOCK TABLES `blog_category` WRITE;
/*!40000 ALTER TABLE `blog_category` DISABLE KEYS */;
INSERT INTO `blog_category` VALUES (1,'Health Tips','Giving you free healthy tips that will significantly improve your knowledge on diet and healthy lifestyle');
/*!40000 ALTER TABLE `blog_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_category_blog`
--

DROP TABLE IF EXISTS `blog_category_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog_category_blog` (
  `blog_category_id` int NOT NULL,
  `blog_id` int NOT NULL,
  PRIMARY KEY (`blog_category_id`,`blog_id`),
  KEY `IDX_3808E168CB76011C` (`blog_category_id`),
  KEY `IDX_3808E168DAE07E97` (`blog_id`),
  CONSTRAINT `FK_3808E168CB76011C` FOREIGN KEY (`blog_category_id`) REFERENCES `blog_category` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_3808E168DAE07E97` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_category_blog`
--

LOCK TABLES `blog_category_blog` WRITE;
/*!40000 ALTER TABLE `blog_category_blog` DISABLE KEYS */;
INSERT INTO `blog_category_blog` VALUES (1,1),(1,2),(1,3);
/*!40000 ALTER TABLE `blog_category_blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `content` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content_text` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (1,'Company Name','company-name','Uzima Well-being Hub'),(2,'Invitation in Slider 1','invite-1','In-home preventive and recovery programs in Nairobi, customized for you.'),(3,'Invitation in Slider 2','invite-2','Proactive, preventive & recovery programs in Nairobi brought right to your doorstep.'),(4,'Slider Button Text','slider-button-cta','Our Services'),(5,'Group Service 1','group-service-1-title','Recovery Programs'),(6,'Group Service 2','group-service-2-title','Preventive Measures'),(7,'Group Service 3','group-service-3-title','Home Health Care'),(8,'Content For Group Service 1','group-service-1-content','Our recovery programs in Nairobi are tailored to individuals who are in need of specialized care and support to regain their health and well-being. \r\n\r\nWhether recovering from arthritis, fibroids, hypertention, or overcoming illness, our programs focus on personalized treatment plans and holistic approaches to promote healing and recovery.'),(9,'Content For Group Service 2','group-service-2-content','Prevention is key to maintaining optimal health and well-being; Our preventive measures are designed to empower individuals with the knowledge, resources, and support they need to proactively manage their health and reduce the risk of illness and disease. \r\n\r\nThis includes, wellness education,  detox, exercise, weight control and more, in Nairobi'),(10,'Content For Group Service 3','group-service-3-content','Our home health care services in Nairobi bring skilled nursing care, medical assistance, and support services directly to your doorstep. \r\n\r\nWhether you or a loved one require post-operative care, medication management, wound care, or assistance with activities of daily living, provide quality care and support in the comfort of your own home.'),(11,'About Us Title','about-us-title','About Uzima Hub'),(12,'About Us Content','about-us-content','At Uzima, we believe in the power of holistic health and personalized care to transform lives. Our mission is to provide comprehensive wellness solutions in Nairobi tailored to your unique needs, right in the comfort of your own home.\r\n\r\nWe\'re passionate about proactive wellness and personalized recovery. Our integrated approach combines preventive measures with specialized recovery programs, all conveniently brought to your doorstep.'),(13,'About Us CTA','about-us-cta','Read More'),(14,'Why Us Title','why-us-title','Why Engage Us?'),(15,'About Us Content for Homepage','why-us-content-hp','Choosing our wellness company for your recovery journey in Nairobi offers a unique advantage by combining the power of food remedies and natural therapies with conventional medicine, resulting in a holistic approach to healing. \r\n\r\nOpting for our services can help you benefit from a speedy recovery while gaining skills on how you can keep from falling sick again by adopting a healthy lifestyle. \r\n\r\nWe can also help you get proactive by keeping yourself healthy.'),(16,'Why Us Img 1 Text','why-us-img-1-text','Recovery'),(17,'Why Us Img 2 Text','why-us-img-2-text','Preventive'),(18,'Why Us Img 3 Text','why-us-img-3-text','Certified'),(19,'Why Us Img 4 Text','why-us-img-4-text','Healthy'),(20,'Services Section Intro','services-homepage-intro','At Uzima, we offer a comprehensive range of services in Nairobi designed to support your health and well-being. \r\n\r\nFrom specialized recovery programs tailored to your individual needs, to preventive measures aimed at maintaining optimal health, and home health care services delivered with compassion and expertise, we\'re committed to providing holistic care that empowers you to thrive. \r\n\r\nWith a focus on natural remedies alongside conventional medicine, our integrated approach addresses both the symptoms and underlying factors contributing to your condition, leading to faster recovery and long-term wellness. \r\n\r\nExplore our services today and take the first step towards a healthier, happier you'),(21,'Products Title','our-products-title','Our Products'),(22,'Products Content For Homepage','our-products-content-hp','We assist in procuring the freshest fruits and natural foods from markets In Nairobi, ensuring you get top-quality produce delivered right to your doorstep. \r\n\r\nAdditionally, we curate effective supplements that we\'ve personally tested, tailored to enhance your well-being. \r\n\r\nFor those seeking detox solutions, we offer expert advice and assistance in sourcing safe-to-use products. \r\n\r\nOur services extend to providing convenient In-home delivery, making healthy living effortless and accessible.'),(23,'Reviews Homepage Intro','reviews-hp-intro','See the power of holistic approach from our client\'s experiences.'),(24,'Our Products CTA','our-products-cta','Call Us'),(25,'Phone Number','phone-number','+254 724 780306'),(26,'Phone Number WhatsApp','phone-number-wa','254724780306'),(27,'Email Address','email-address','info@uzimahub.co.ke'),(28,'CTA Section Title','cta-title','Reach Out'),(29,'CTA Section Content','cta-content','Ready to embark on your journey to better health? Contact us today to learn more about our services and schedule your personalized consultation. \r\n\r\nWhether you\'re recovering from illness, seeking preventive measures, or in need of home health care, our team is here to support you every step of the way. \r\n\r\nTake the first step towards a healthier, happier you—reach out to us now!\"'),(30,'Phone CTA','phone-cta','Call Us'),(31,'WhatsApp CTA','whatsapp-cta','WhatsApp Us'),(32,'Contact Form CTA','contact-us-cta','Contact Form'),(33,'Location','location','In-home Natural Health Consultancy, Nairobi, Kenya'),(34,'Invitation Text','invite-text','Subscribe for Tele-care and More Benefits');
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `faq` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question` varchar(255) DEFAULT NULL,
  `answer` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
INSERT INTO `faq` VALUES (1,'Who are you?','<div>We\'re an organization passionate about proactive wellness and personalized recovery. Our integrated approach combines preventive measures with specialized recovery programs, all conveniently brought to your doorstep.<br><br></div><div>We believe in the power of holistic health and personalized care to transform lives. Our mission is to provide comprehensive wellness solutions in Nairobi tailored to your unique needs, right in the comfort of your own home.</div>'),(2,'What types of conditions do your recovery programs address?','<div>Our recovery programs are designed to address a wide range of conditions, including chronic illnesses, arthritis, hypertension, cancer and more. We tailor each program to meet the individual needs of our clients.</div>'),(3,'How do natural remedies complement conventional medicine in your approach?','<div>Natural remedies, such as herbal supplements, dietary changes, and mind-body practices, can complement conventional medicine by enhancing its effectiveness, reducing side effects, and promoting overall well-being. We integrate these remedies into our treatment plans to provide a holistic approach to healing.</div>'),(4,'What preventive measures do you offer to maintain optimal health?','<div>Our preventive measures include health screenings, wellness education, lifestyle interventions, and more. These services are designed to empower individuals to proactively manage their health and reduce the risk of illness and disease.</div>'),(5,'What services are included in your home health care offerings?','<div>Our home health care services encompass a wide range of medical and support services, including tele-health care skilled nursing care, medication management, wound care, assistance with activities of daily living, and companionship services. We provide personalized care in the comfort and familiarity of your own home.</div>'),(6,'How do I know if your services are right for me?','<div>We offer personalized consultations to assess your needs and discuss your treatment options. Our team will work with you to develop a customized care plan that meets your individual health goals and preferences.</div>'),(7,'How can I schedule an appointment or consultation?','<div>To schedule an appointment or consultation, simply <a href=\"https://uzimahub.co.ke/contact-uzima-well-being-hub\">contact us via phone, email, or our online contact form</a>. Our friendly staff will be happy to assist you and answer any questions you may have.</div>');
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `image` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (1,'Slider Image 1','slider-image-1','uzima-hub-slider-1-1711860045.jpg'),(2,'Slider Image 2','slider-image-2','uzima-hub-slider-2-1711860264.jpg');
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `message` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` longtext,
  `received_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messenger_messages`
--

DROP TABLE IF EXISTS `messenger_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messenger_messages` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `body` longtext NOT NULL,
  `headers` longtext NOT NULL,
  `queue_name` varchar(190) NOT NULL,
  `created_at` datetime NOT NULL,
  `available_at` datetime NOT NULL,
  `delivered_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_75EA56E0FB7336F0` (`queue_name`),
  KEY `IDX_75EA56E0E3BD61CE` (`available_at`),
  KEY `IDX_75EA56E016BA31DB` (`delivered_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messenger_messages`
--

LOCK TABLES `messenger_messages` WRITE;
/*!40000 ALTER TABLE `messenger_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messenger_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `page` (
  `id` int NOT NULL AUTO_INCREMENT,
  `heading` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `featured_image` varchar(255) DEFAULT NULL,
  `page_content` longtext,
  `content_first` tinyint(1) DEFAULT NULL,
  `is_service` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,'In-home Natural Health Consultancy','Holistic Healthy Living Coach','Would you like to avoid or overcome lifestyle diseases? We have natural ways to enhance your well-being. Begin a holistic healthy living in Nairobi at home.','natural-healthy-living','uzima-logo-square-white-bg-1711436697.jpg',NULL,0,0),(2,'About Uzima Well-being Hub','About Us - Uzima Well-being Hub','The cost of preventing disease is much lower compared to the cost of curing disease. We are here to help you keep healthy.','about-uzima-well-being-hub','uzima-logo-square-white-bg-1711464152.jpg',NULL,0,0),(3,'Healthy Living Products','Uzima Well-being Products','From detoxing to supplements, Uzima Well-being Hub is armed to the teeth to ensure you are one healthy person.','our-products','uzima-logo-square-white-bg-1711466702.jpg',NULL,0,0),(4,'Our Services - Uzima Well-being Hub','What We Do At Uzima Well-being Hub','Our in-home services range from caring for the sick to maintaining good health at very affordable monthly fees.','our-services','uzima-logo-square-white-bg-1711467611.jpg',NULL,0,0),(5,'Contact Uzima Well-being Hub','Our Contacts - Uzima Well-being Hub','Reach out to us for personalized experience as you journey towards improving your health, or recovering from various conditions.','contact-uzima-well-being-hub','uzima-logo-square-white-bg-1711604208.jpg',NULL,0,0),(6,'Uzima Blog','Blog Articles - Uzima Well-being Hub','We write natural health content that will help you understand your body better and improve your health.','blog-articles','uzima-logo-square-white-bg-1711607970.jpg',NULL,0,0),(7,'Blog Content','Blog Content','Blog Content','blog-content','uzima-logo-square-white-bg-1711633616.jpg',NULL,0,0),(8,'In-Home Cancer Care in Nairobi','In-Home Cancer Care in Nairobi - Uzima Well-being Hub','Explore natural cancer management strategies: pain relief, breast, prostate, lung, bone cancers, chemotherapy, leukemia, ovarian care. Expert support for holistic patient journeys.','in-home-cancer-care-nairobi','home-cancer-care-1519-x-550-1711714842.jpg','<h2>Customized Home Care For Cancer Patients in Nairobi</h2>\r\n\r\n<p>At Uzima Well-being Hub, we understand the unique challenges faced by cancer patients receiving treatment at home in Nairobi. We offer a comprehensive range of in-home services designed to empower and support you throughout your cancer journey.</p>\r\n\r\n<h2>How We Can Help:</h2>\r\n\r\n<h3>Breast Pain Treatment</h3>\r\n\r\n<p>We understand that breast pain can be caused by various factors, including cancer. Our team of specialists can help identify the cause of your pain and develop a personalized treatment plan to alleviate it, improving your comfort and well-being.</p>\r\n\r\n<h3>Esophageal Cancer Management</h3>\r\n\r\n<p>Our team can provide in-home support for individuals battling esophageal cancer. This includes symptom management, nutritional counseling, and emotional support to help you navigate the challenges of the disease.</p>\r\n\r\n<h3>Overall Cancer Management</h3>\r\n\r\n<p>We offer a holistic approach to cancer care, working alongside your oncologist to provide in-home support services that complement your treatment plan.</p>\r\n\r\n<h3>Cancer Pain Management &amp; Relief</h3>\r\n\r\n<p>Managing pain is a crucial aspect of cancer care. We offer in-home pain management services, including medication management and complementary therapies like massage, to help you achieve significant pain relief and improve your quality of life.</p>\r\n\r\n<h3>Management of Specific Cancers</h3>\r\n\r\n<p>Our team has experience supporting individuals with various cancers, including breast cancer and prostate cancer, . We can provide specialized care plans tailored to the unique needs of each condition.</p>\r\n\r\n<h2>Additional Services</h2>\r\n\r\n<p>We have several Cancer Care services including but not limited to:</p>\r\n\r\n<p>Cancer Pain Management,&nbsp;Breast Cancer Management,&nbsp;Prostate Cancer Management,&nbsp;Home Care,&nbsp;Pain Relief,&nbsp;Lung Cancer,&nbsp;Bone Cancer,&nbsp;Metastatic Cancer,&nbsp;Chemotherapy Side Effects Management,&nbsp;Palliative Care,&nbsp;Cancer Pain Management,&nbsp;Breast Cancer Treatment,&nbsp;Chemotherapy Side Effects,&nbsp;Prostate Cancer Management,&nbsp;Lung Cancer Pain Relief,&nbsp;Bone Cancer Treatment,&nbsp;Leukemia Management,&nbsp;Ovarian Cancer Care,&nbsp;Colorectal Cancer Management,&nbsp;Palliative Care in Cancer.</p>\r\n\r\n<h3>Nutritional Counseling</h3>\r\n\r\n<p>Our experienced nutritionists can design personalized nutrition plans to optimize your strength and well-being during treatment and recovery.</p>\r\n\r\n<h3>Psychosocial Support</h3>\r\n\r\n<p>We offer compassionate counseling and support groups for you and your loved ones to navigate the emotional challenges of cancer.</p>\r\n\r\n<h3>Symptom Management</h3>\r\n\r\n<p>We provide evidence-based strategies to manage common cancer symptoms like fatigue, nausea, and insomnia, improving your comfort at home.</p>\r\n\r\n<h3>Integrative Oncology Consultations</h3>\r\n\r\n<p>We explore complementary therapies alongside conventional treatments to enhance overall well-being.</p>\r\n\r\n<h3>End-of-Life Care</h3>\r\n\r\n<p>Our team offers compassionate end-of-life care and palliative support services focused on symptom management, pain relief, and emotional support for you and your loved ones.</p>\r\n\r\n<h3>Contact Uzima Well-being Hub Today</h3>\r\n\r\n<p>Don&#39;t face cancer alone. Let Uzima Well-being Hub be your partner in in-home care. Contact us today to learn more about our services and how we can support your journey towards healing and hope.</p>',1,1),(9,'In-Home Arthritis Recovery Program in Nairobi','In-Home Arthritis Recovery Program at Your Home','Discover our in-home arthritis recovery program in Nairobi. Personalized care for managing rheumatoid arthritis, osteoarthritis, joint pain relief, and holistic therapy','in-home-arthritis-recovery-nairobi','arthritis-recovery-uzima-wellbeing-hub-1711722830.jpg','<h2>Welcome to Our In-Home Arthritis Recovery Program in Nairobi</h2>\r\n\r\n<p>At Uzima Well-being Hub, we&#39;re committed to bringing comprehensive arthritis recovery services right to your doorstep in Nairobi. Our In-Home Arthritis Recovery Program is designed to help you manage your symptoms, improve mobility, and enhance your quality of life&mdash;all from the comfort of your own home. Whether you&#39;re dealing with rheumatoid arthritis, osteoarthritis, or other forms of arthritis, our program offers personalized care, evidence-based therapies, and holistic approaches to support your journey to recovery.</p>\r\n\r\n<p>Our In-Home Arthritis Recovery Program Features:</p>\r\n\r\n<h3>Comprehensive Assessment</h3>\r\n\r\n<p>We start with a thorough evaluation of your arthritis condition, taking into account your medical history, symptoms, and lifestyle factors to develop a personalized recovery plan tailored to your needs.</p>\r\n\r\n<h3>Arthritis Self-Care Education</h3>\r\n\r\n<p>Empower yourself with knowledge about arthritis self-care techniques to manage your condition effectively. Learn practical tips and strategies for managing rheumatoid arthritis and osteoarthritis symptoms, including joint protection techniques, stress management, and lifestyle modifications.</p>\r\n\r\n<h3>Medication Management</h3>\r\n\r\n<p>Our team provides guidance on the best natural pain relief options for knee arthritis and other joint pain, including natural medication management strategies tailored to your specific needs and preferences.</p>\r\n\r\n<h3>Physical Therapy and Exercise Programs</h3>\r\n\r\n<p>Improve joint function, flexibility, and strength with personalized exercise programs designed by our certified physical therapists. We bring physical therapy sessions directly to your home, making it convenient for you to stay active and mobile.</p>\r\n\r\n<h3>Rheumatoid Arthritis Therapy Treatment</h3>\r\n\r\n<p>Our program includes specialized therapy treatments for managing rheumatoid arthritis, focusing on reducing inflammation, preventing joint damage, and improving overall joint health.</p>\r\n\r\n<h3>Holistic Pain Relief</h3>\r\n\r\n<p>Explore holistic approaches to pain relief, including acupuncture, massage therapy, and herbal supplements, all provided in the comfort of your own home in Nairobi.</p>\r\n\r\n<h3>Supportive Care and Emotional Well-being</h3>\r\n\r\n<p>Our team offers compassionate support to help you cope with the emotional challenges of living with arthritis. Join our virtual support groups, connect with others facing similar challenges, and access resources to enhance your emotional well-being.</p>\r\n\r\n<h3>Ongoing Monitoring and Adjustments</h3>\r\n\r\n<p>We monitor your progress closely and make adjustments to your treatment plan as needed to ensure you&#39;re receiving the most effective care possible.</p>\r\n\r\n<h2>Experience the Convenience of In-Home Arthritis Recovery Care</h2>\r\n\r\n<p>Say goodbye to the hassle of traveling to appointments. With our In-Home Arthritis Recovery Program, you can receive high-quality care and support right in your own home. Contact us today to learn more about how we can help you manage your arthritis symptoms and improve your quality of life. Let&#39;s take the first step toward a healthier, more comfortable future together.</p>',1,1),(10,'In-Home Fibroid Recovery Program in Nairobi','n-Home Fibroid Recovery Program at Home','Welcome to our in-home fibroid recovery program in Nairobi. Personalized care for uterine fibroid management and holistic pain relief. Start your journey today!','in-home-fibroid-recovery-program-nairobi','fibroid-recovery-uzima-wellbeing-hub-1711725736.jpg','<div style=\"page-break-after:always\"><span style=\"display:none\">&nbsp;</span></div>\r\n\r\n<p>Image by <a href=\"https://www.freepik.com/free-photo/hand-holding-uterus-ovary-model_24799388.htm#fromView=search&amp;page=1&amp;position=5&amp;uuid=cfef1c44-aa96-4849-a6c2-65c46c6ab4a3\" target=\"_blank\">Freepik</a></p>\r\n\r\n<h2>Welcome to Our In-Home Fibroid Recovery Program in Nairobi</h2>\r\n\r\n<p>At Uzima Well-being Hub, we understand the challenges and discomfort that fibroids can bring to your life. That&#39;s why we&#39;re dedicated to providing a comprehensive In-Home Fibroid Recovery Program right here in Nairobi. Our program is designed to help you manage symptoms, promote healing, and improve your overall well-being&mdash;all from the comfort of your own home. Combining evidence-based treatments, personalized care, and holistic approaches, we&#39;re here to support your journey to fibroid recovery.</p>\r\n\r\n<h3>Our In-Home Fibroid Recovery Program Features</h3>\r\n\r\n<p>Uterine Fibroid Management Our program begins with a thorough assessment of your fibroid condition, considering factors like size, location, and symptoms to create a personalized recovery plan tailored to your needs.</p>\r\n\r\n<h3>Medical Management</h3>\r\n\r\n<p>Our experienced team provides guidance on medical treatments and options for managing fibroid symptoms, including natural remedies, pain management and earth therapies.</p>\r\n\r\n<h3>Nutritional Counseling<strong> </strong></h3>\r\n\r\n<p>Learn how diet and nutrition can play a crucial role in managing fibroid symptoms and promoting healing. Our registered dietitians offer personalized nutritional counseling to support your recovery journey.</p>\r\n\r\n<h3>Fibroid Pain Management<strong> </strong></h3>\r\n\r\n<p>Coping with fibroid-related pain is challenging. We offer a range of natural pain management strategies and interventions to help alleviate discomfort and enhance your quality of life.</p>\r\n\r\n<h3>Holistic Therapies<strong> </strong></h3>\r\n\r\n<p>Experience the benefits of holistic therapies such as massage therapy, and stress-reduction techniques, all in the comfort of your own home, to support your overall well-being and promote healing.</p>\r\n\r\n<h3>Emotional Support<strong> </strong></h3>\r\n\r\n<p>Dealing with fibroids can take a toll emotionally. Our compassionate team offers support and guidance to help you navigate the emotional challenges of living with fibroids.</p>\r\n\r\n<h3>Educational Resources<strong> </strong></h3>\r\n\r\n<p>Empower yourself with knowledge about fibroids and your treatment options. Access educational resources, articles, and workshops to learn more about fibroid management and recovery.</p>\r\n\r\n<h3>Follow-Up Care<strong> </strong></h3>\r\n\r\n<p>We provide ongoing monitoring and follow-up care to ensure that your treatment plan is effective and to make any necessary adjustments to support your recovery.</p>\r\n\r\n<h2>Start Your Journey to In-Home Fibroid Recovery Today</h2>\r\n\r\n<p>Don&#39;t let fibroids control your life. Our In-Home Fibroid Recovery Program is here to support you every step of the way. Contact us today to learn more about how we can help you manage your symptoms, promote healing, and improve your overall well-being&mdash;all from the comfort of your own home. Let&#39;s work together to reclaim your health and vitality.</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p>&nbsp;</p>',1,1),(11,'FAQ','Frequently Asked Questions - Uzima Well-being Hub','Our in-home services range from caring for the sick to maintaining good health at very affordable monthly fees.','faq','uzima-long-logo-transparent-bg-1711435495-1711964882.png',NULL,0,0),(12,'Customized Diet Programs for Weight Loss','Achieve Your Weight Loss Goals with Our Customized Diet Programs','Weight Loss? Our personalized diet programs can help you achieve sustainable weight loss and improved health. Start your journey today!','personalized-weight-loss-diet-program','diet-program-weight-loss-1711971018.jpg','<p>Embarking on a weight loss journey can be daunting, but with the right guidance and support, it&#39;s entirely achievable.</p>\r\n\r\n<p>At Uzima Well-being Hub, we&#39;re dedicated to helping you reach your weight loss goals through our personalized diet programs.</p>\r\n\r\n<p>Our programs are designed to provide you with the tools, resources, and motivation you need to make <strong>lasting changes</strong> to your eating habits and lifestyle, resulting in sustainable weight loss and improved overall health.</p>\r\n\r\n<h2>Our Approach</h2>\r\n\r\n<p>Our approach to weight loss focuses on creating customized diet plans that are tailored to your individual needs, preferences, and goals. We understand that everyone&#39;s body is unique, which is why we take the time to <strong>assess</strong> your specific dietary requirements and design a program that works best for you.</p>\r\n\r\n<p>Our diet programs emphasize whole, nutrient-dense foods that nourish your body and support your weight loss efforts.</p>\r\n\r\n<p>We prioritize balanced meals that are rich in lean proteins, healthy fats, fiber, and essential vitamins and minerals.</p>\r\n\r\n<p>We ensure to suggest foods that are readily available in the Nairobi Market.</p>\r\n\r\n<p>For those who need assistance in acquiring the foods, we provide contacts for people who farm organic foods.</p>\r\n\r\n<p>By fueling your body with high-quality, wholesome ingredients, you&#39;ll feel satisfied, energized, and motivated to stick to your weight loss plan.</p>\r\n\r\n<h2>Key Components</h2>\r\n\r\n<h3>Personalized Meal Plans</h3>\r\n\r\n<p>Receive customized meal plans that are tailored to your dietary preferences, lifestyle, and weight loss goals.</p>\r\n\r\n<p>Our dieticians work closely with you to create balanced meals that are delicious, satisfying, and conducive to weight loss.</p>\r\n\r\n<h3>Nutritional Guidance</h3>\r\n\r\n<p>Gain valuable insights into nutrition and healthy eating habits through one-on-one consultations with our experienced dieticians.</p>\r\n\r\n<p>Learn how to make informed food choices, navigate dining out options, and overcome common challenges that may arise during your weight loss journey.</p>\r\n\r\n<h3>Ongoing Support</h3>\r\n\r\n<p>Stay motivated and accountable with ongoing support from our team of wellness professionals.</p>\r\n\r\n<p>Whether you have questions, need encouragement, or require adjustments to your meal plan, we&#39;re here to provide guidance and support every step of the way.</p>\r\n\r\n<h3>Lifestyle Modification</h3>\r\n\r\n<p>In addition to dietary changes, we&#39;ll help you implement sustainable lifestyle modifications that support your weight loss goals.</p>\r\n\r\n<p>From incorporating regular physical activity into your routine to practicing stress management techniques, we&#39;ll help you adopt healthy habits that promote long-term success.</p>\r\n\r\n<h2>Benefits of Our Diet Programs</h2>\r\n\r\n<h3>Sustainable Weight Loss</h3>\r\n\r\n<p>Achieve your weight loss goals in a healthy, sustainable manner that promotes long-term success.</p>\r\n\r\n<h3>Improved Health</h3>\r\n\r\n<p>Experience numerous health benefits, including reduced risk of chronic diseases, improved blood sugar control, and enhanced overall well-being.</p>\r\n\r\n<h3>Increased Energy and Vitality</h3>\r\n\r\n<p>Feel more energetic, vibrant, and confident as you shed excess weight and improve your health.</p>\r\n\r\n<h2>Weight Management Programs:</h2>\r\n\r\n<h3>Nutritional Counseling for Weight Loss</h3>\r\n\r\n<p>Guidance on healthy eating habits, portion control, and nutrient-dense foods to support weight loss goals.</p>\r\n\r\n<h3>Fitness Training for Weight Loss</h3>\r\n\r\n<p>Customized workout plans focusing on cardiovascular exercises, strength training, and high-intensity interval training (HIIT) to burn calories and build muscle mass.</p>\r\n\r\n<h3>Meal Planning Services</h3>\r\n\r\n<p>Weekly or monthly meal plans designed specifically for weight loss, including recipes, shopping lists, and nutritional information.</p>\r\n\r\n<h3>Behavioral Therapy</h3>\r\n\r\n<p>Counseling sessions to address emotional eating, stress management, and other psychological factors that may contribute to weight gain.</p>\r\n\r\n<h3>Group Weight Loss Challenges</h3>\r\n\r\n<p>Organizing group challenges or competitions to motivate participants, foster accountability, and celebrate achievements together.</p>\r\n\r\n<h3>Body Composition Analysis</h3>\r\n\r\n<p>Assessing body fat percentage, muscle mass, and other key metrics to track progress and adjust weight loss strategies accordingly.</p>\r\n\r\n<h3>Support Groups</h3>\r\n\r\n<p>Creating a supportive community where individuals can share experiences, provide encouragement, and receive guidance from peers and wellness professionals.</p>\r\n\r\n<h3>Healthy Cooking Workshops</h3>\r\n\r\n<p>Interactive workshops teaching participants how to prepare nutritious meals and snacks that support weight loss goals.</p>\r\n\r\n<h3>Supplement Recommendations</h3>\r\n\r\n<p>Providing information on natural supplements and vitamins that may aid in weight loss when combined with a healthy diet and exercise regimen.</p>\r\n\r\n<h3>Take the First Step Towards Your Weight Loss Goals</h3>\r\n\r\n<p>Are you ready to take control of your health and transform your body?</p>\r\n\r\n<p>Contact us today to learn more about our customized diet programs and schedule your initial consultation.</p>\r\n\r\n<p>Together, we&#39;ll create a personalized plan that empowers you to achieve lasting weight loss success.</p>\r\n\r\n<p>Your journey to a healthier, happier you starts now!</p>',1,1),(13,'Pricing - Uzima Well-being Hub','Our Subscription and Services Pricing','Discover transparent pricing for our health services, including tele-care subscriptions, in-person visits, and home care. Get quality care at competitive rates','uzima-pricing','uzima-logo-square-white-bg-1711974158.jpg','<p>At Uzima Well-being Hub, we offer transparent and competitive pricing for our range of services aimed at providing you with the support and care you need.</p>\r\n\r\n<p>Below are our basic pricing options:</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p><strong>Tele-Care Subscription and Membership:</strong></p>\r\n\r\n	<ul>\r\n		<li>Cost: KSh 15,000 per month</li>\r\n		<li>Description: With our Tele-Care Subscription and Membership, you&#39;ll gain access to personalized health guidance and all our online content concerning various health needs and detailed preparation and usage information, and support from our team of experts, all from the comfort of your own home. Enjoy unlimited consultations, regular check-ins, and ongoing support to help you achieve your health goals.</li>\r\n	</ul>\r\n	</li>\r\n	<li>\r\n	<p><strong>In-Person Visits:</strong></p>\r\n\r\n	<ul>\r\n		<li>Cost: KSh 2,500 per visit</li>\r\n		<li>Description: For those preferring in-person consultations, we offer individual visits at a competitive rate. Receive personalized care, training&nbsp;and support during each visit to address your specific health needs and concerns.</li>\r\n	</ul>\r\n	</li>\r\n	<li>\r\n	<p><strong>Home Care Services:</strong></p>\r\n\r\n	<ul>\r\n		<li>Cost: KSh 4,000 per day</li>\r\n		<li>Description: Our Home Care Services provide comprehensive care and support for individuals requiring assistance with activities of daily living, medical treatments, and more. Our trained professionals will ensure your comfort and well-being while providing quality care in the comfort of your home.</li>\r\n	</ul>\r\n	</li>\r\n</ol>\r\n\r\n<p>Please note that prices are subject to change and may vary depending on the specific services required. For more information or to schedule a consultation, please <a href=\"https://uzimahub.co.ke/contact-uzima-well-being-hub\">contact us.</a></p>\r\n\r\n<p>We understand that navigating healthcare costs can be challenging, which is why we&#39;re committed to providing transparent pricing and exceptional value for our services.</p>\r\n\r\n<p>Let us support you on your journey to better health and well-being.</p>',1,0),(14,'Comprehensive Home Health Care Services Tailored to Your Needs','Home Health Care Services Tailored to Your Needs','Get compassionate home health care tailored to your needs. Quality care in the comfort of home. Contact us today. Your well-being matters','home-health-care-uzima','uzima-home-health-care-1711979493.jpg','<p>At Uzima Well-being Hub, we understand that recovering from illness or managing chronic conditions can be challenging, especially when faced with the need for medical care.</p>\r\n\r\n<p>That&#39;s why we offer comprehensive Home Health Care services designed to provide you with personalized care and support in the comfort of your own home.</p>\r\n\r\n<p>Whether you require assistance with activities of daily living, medication management, or specialized medical treatments, our team of compassionate professionals is here to ensure your well-being and comfort every step of the way.</p>\r\n\r\n<h2>Our Home Health Care Services</h2>\r\n\r\n<h3>Skilled Nursing Care</h3>\r\n\r\n<p>Receive expert nursing care from our qualified professionals, including wound care, medication administration, monitoring of vital signs, and coordination of care with your healthcare team.</p>\r\n\r\n<h3>Personal Care Assistance</h3>\r\n\r\n<p>Get assistance with activities of daily living, such as bathing, dressing, grooming, toileting, and mobility assistance, to help you maintain independence and dignity at home.</p>\r\n\r\n<h3>Medication Management</h3>\r\n\r\n<p>Stay on track with your medications with our medication management services, including medication reminders, assistance with medication administration, and monitoring for side effects or adverse reactions.</p>\r\n\r\n<h3>Chronic Disease Management</h3>\r\n\r\n<p>Manage chronic conditions such as diabetes, heart disease, COPD, and more with our specialized care plans tailored to your individual needs, including education, monitoring, and support to optimize your health outcomes.</p>\r\n\r\n<h3>Post-Surgical Care</h3>\r\n\r\n<p>Recover comfortably at home following surgery with our post-surgical care services, including wound care, pain management, mobility assistance, and assistance with activities of daily living to support your recovery process.</p>\r\n\r\n<h3>Pain Management</h3>\r\n\r\n<p>Receive personalized pain management strategies and interventions to alleviate discomfort and improve your quality of life, including non-pharmacological approaches such as massage therapy, heat therapy, and relaxation techniques.</p>\r\n\r\n<h3>End-of-Life Care and Palliative Support</h3>\r\n\r\n<p>Receive compassionate end-of-life care and palliative support services for individuals with advanced or terminal illnesses, focusing on pain relief, symptom management, emotional support, and assistance with advance care planning.</p>\r\n\r\n<h2>Why Choose Our Home Health Care Services?</h2>\r\n\r\n<h3>Comfort and Convenience</h3>\r\n\r\n<p>Receive quality care and support in the comfort and familiarity of your own home, where you can maintain your routines and independence.</p>\r\n\r\n<h3>Personalized Care</h3>\r\n\r\n<p>Our services are tailored to your individual needs, preferences, and goals, ensuring that you receive the level of care and support that is right for you.</p>\r\n\r\n<h3>Peace of Mind</h3>\r\n\r\n<p>Rest assured knowing that you or your loved one is in the hands of experienced professionals who are dedicated to providing compassionate, high-quality care.</p>\r\n\r\n<h3>Take the Next Step Towards Home Health Care</h3>\r\n\r\n<p>Are you or a loved one in need of home health care services?</p>\r\n\r\n<p>Contact us today to learn more about our comprehensive care options and schedule a consultation.</p>\r\n\r\n<p>Let us support you on your journey to health and well-being, right in the comfort of your own home.</p>\r\n\r\n<p>Experience the comfort and convenience of home health care with Uzima Well-being Hub.</p>\r\n\r\n<p>Your well-being is our priority.</p>',1,1);
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_section`
--

DROP TABLE IF EXISTS `page_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `page_section` (
  `page_id` int NOT NULL,
  `section_id` int NOT NULL,
  PRIMARY KEY (`page_id`,`section_id`),
  KEY `IDX_D713917AC4663E4` (`page_id`),
  KEY `IDX_D713917AD823E37A` (`section_id`),
  CONSTRAINT `FK_D713917AC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D713917AD823E37A` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_section`
--

LOCK TABLES `page_section` WRITE;
/*!40000 ALTER TABLE `page_section` DISABLE KEYS */;
INSERT INTO `page_section` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(2,4),(2,7),(2,9),(2,17),(2,22),(3,7),(3,11),(3,17),(4,7),(4,10),(4,12),(5,9),(5,13),(5,14),(5,15),(5,17),(6,7),(6,16),(6,17),(7,18),(7,19),(11,17),(11,20),(11,21),(13,21);
/*!40000 ALTER TABLE `page_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reset_password_request`
--

DROP TABLE IF EXISTS `reset_password_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reset_password_request` (
  `id` int NOT NULL AUTO_INCREMENT,
  `selector` varchar(20) NOT NULL,
  `hashed_token` varchar(100) NOT NULL,
  `requested_at` datetime NOT NULL,
  `expires_at` datetime NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7CE748AA76ED395` (`user_id`),
  CONSTRAINT `FK_7CE748AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reset_password_request`
--

LOCK TABLES `reset_password_request` WRITE;
/*!40000 ALTER TABLE `reset_password_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `reset_password_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `section` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `template_path` varchar(255) DEFAULT NULL,
  `priority` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section`
--

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
INSERT INTO `section` VALUES (1,'Home slider and nav bar','slider.html.twig',1),(2,'Our Qualifications Section','qualities.html.twig',2),(3,'Introduction - About Us','about.html.twig',3),(4,'Why Choose Us Section','why_us.html.twig',4),(5,'Our Services - Homepage Section','services.html.twig',5),(6,'Introduction to Our Products - Homepage Section','products_intro.html.twig',6),(7,'Client Reviews/testimonial section','testimonials.html.twig',7),(8,'Our qualifications for about us page','qualities_about.html.twig',4),(9,'Call to action for about us page','cta.html.twig',5),(10,'Static Slider for other pages, not homepage margin - 90','page_slider_90.html.twig',1),(11,'Products Listing on the products page','products_pg_list.html.twig',2),(12,'Service Listing on Services Page','services_pg_list.html.twig',2),(13,'Contact Form','form.html.twig',8),(14,'Contacts - phone, email, location','contact.html.twig',9),(15,'Map Section','map.html.twig',10),(16,'List of blog posts','blog_posts.html.twig',2),(17,'Static Slider for other pages, not homepage margin - 0','page_slider_0.html.twig',1),(18,'Blog Content Section','blog_article.html.twig',2),(19,'Blog Slider','page_slider_article.html.twig',1),(20,'Frequently Asked Questions','faq.html.twig',5),(21,'Call To Action For FAQ Page','cta.html.twig',12),(22,'Our Qualifications with Margin Top','qualities90.html.twig',4);
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo`
--

DROP TABLE IF EXISTS `seo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `heading` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `logo_path` varchar(255) DEFAULT NULL,
  `keywords` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo`
--

LOCK TABLES `seo` WRITE;
/*!40000 ALTER TABLE `seo` DISABLE KEYS */;
INSERT INTO `seo` VALUES (1,'In-home Natural Health Consultancy','Holistic Healthy Living Coach','Would you like to avoid or overcome lifestyle diseases? We have natural ways to enhance your well-being. Begin a holistic healthy living in Nairobi at home.','uzima-long-logo-transparent-bg-1711435495.png','Healthy living, Natural wellness, Holistic health, Home wellness, Nairobi, Eco-friendly, Organic living, Lifestyle tips, Healthy home, Wellness consultant,');
/*!40000 ALTER TABLE `seo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_member`
--

DROP TABLE IF EXISTS `team_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `team_member` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_member`
--

LOCK TABLES `team_member` WRITE;
/*!40000 ALTER TABLE `team_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `testimonials` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
INSERT INTO `testimonials` VALUES (1,'Dennis','user-1711876912.png','Client','My mother was already scheduled for knee surgery to replace her knee caps with metallic ones when we learned about Uzima. We received guidance from them on how to reduce knee pain using various therapies and calcium supplements, and as a result, we were able to avoid the surgery. Two months later, my mum is now walking without crutches.'),(2,'Eva','user-1711877166.png','Client','My dad had used blood-pressure pills for over 3 years. The pills worked very well for him. We never thought, however, that there was any other way to regulate pressure apart from using the pills. When we were referred to Uzima, we engaged them just to help dad get a good diet, but we got more than that. Dad has been taken off the pills after using some dandelion powder along some other foods and juices.'),(3,'Isaac','user-1711877392.png','Client','I was diagnosed with early stage esophagus cancer which resulted from my body reacting to glutten. I used a lot of money to get treatments, but Uzima gave me some cheap ways of recovering from the tumors. The use of readily available foods from the market has made the treatment very affordable, and the training to prepare these juices have given me power and independence in managing and recovering from this situation. I truly appreciate Uzima.');
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(180) NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `usertype` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `about` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'maestrojosiah@gmail.com','[\"ROLE_ADMIN\"]','$2y$13$q8nBW/6XCyLYvYqOOAZCeuLbK6jOIUrCYq5Mvmu8jrwX6Pqq9X25.','Josiah','Birai',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-01 17:57:11
